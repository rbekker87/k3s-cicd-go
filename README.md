# k3s-cicd-go

Resources:

- https://medium.com/better-programming/using-a-k3s-kubernetes-cluster-for-your-gitlab-project-b0b035c291a9
- https://docs.bitnami.com/tutorials/create-ci-cd-pipeline-gitlab-kubernetes/
- https://www.padok.fr/en/blog/kubernetes-gitlab-pipeline
- https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/
- https://dzone.com/articles/blog-how-to-setup-a-cicd-pipeline-with-kubernetes
- https://github.com/AlibabaCloudDocs/csk/blob/master/intl.en-US/Best%20Practices/DevOps/Use%20GitLab%20CI%20to%20run%20a%20GitLab%20runner%20and%20activate%20a%20pipleline%20in%20a%20Kubernetes%20cluster.md
- https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/