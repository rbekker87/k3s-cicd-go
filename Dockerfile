FROM golang:1.11-alpine AS builder

WORKDIR /usr/build
ADD main.go .
RUN go build -o app .

FROM alpine:latest
WORKDIR /usr/src
COPY --from=builder /usr/build/app .

EXPOSE 5000

CMD ["/usr/src/app"]